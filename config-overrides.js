const { alias } = require('react-app-rewire-alias')

module.exports = function override(config) {
  alias({
    '@': 'src',
    '@pages': 'src/pages',
    '@components': 'src/components',
    '@api': 'src/api',
    '@mocks': 'src/__mocks__',
    '@utils': 'src/utils',
  })(config)

  return config
}
