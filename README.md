# Requeriments

- Yarn Global
- Node v14.18.3

## Enviroment variables

- it's necessary declare the variables in a file called **_.env.local_**
  - These are the necessary variables

```
REACT_APP_SPOTIFY_CLIENT_ID=
REACT_APP_SPOTIFY_CLIENT_SECRET=
REACT_APP_SPOTIFY_CALLBACK_HOST=
```

## Install

- It's necessary to install the dependencies for continues

```
  yarn
```

## Execute

- Execute in local

```
  yarn start
```

- Build project

```
  yarn build
```

- Execute test

```
  yarn test
```
