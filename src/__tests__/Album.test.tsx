import { render, screen } from '@testing-library/react'
import Album from '@pages/Album'
import albumSingle from '@mocks/api/albumSingle.json'

const mockedNavigate = jest.fn()

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedNavigate,
}))

describe('<Album />', () => {
  beforeAll(() => jest.spyOn(window, 'fetch'))

  it('should render the alnum page', async () => {
    window.fetch = jest.fn().mockResolvedValueOnce({
      ok: true,
      json: async () => albumSingle,
    })

    render(<Album />)
    expect(window.fetch).toHaveBeenCalledTimes(1)
    expect(await screen.findByText('La Historia Sin Fin')).toBeInTheDocument()
  })
})
