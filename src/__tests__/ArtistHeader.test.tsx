import { render, screen } from '@testing-library/react'
import ArtistHeader from '@components/ArtistHeader'
import artistData from '@mocks/api/artist.json'

const mockedNavigate = jest.fn()

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedNavigate,
}))

describe('<ArtistHeader />', () => {
  beforeAll(() => jest.spyOn(window, 'fetch'))

  it('should render the artist info', async () => {
    window.fetch = jest.fn().mockResolvedValueOnce({
      ok: true,
      json: async () => artistData,
    })

    render(<ArtistHeader id={artistData.id} />)

    expect(window.fetch).toHaveBeenCalledTimes(1)
    expect(await screen.findByText(artistData.name)).toBeInTheDocument()
  })
})
