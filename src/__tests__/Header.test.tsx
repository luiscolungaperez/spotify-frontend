import { screen, render } from '@testing-library/react'
import Header from '@components/Header'
import UserData from '@mocks/api/user.json'

describe('<Header />', () => {
  it('should render user button', () => {
    render(<Header user={UserData} />)
    expect(screen.getByText('luiscolungaperez')).toBeInTheDocument()
  })
})
