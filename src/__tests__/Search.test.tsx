import { render, screen } from '@testing-library/react'
import Search from '@components/Search'

describe('<Search />', () => {
  it('should render the search component', () => {
    render(<Search />)
    expect(screen.getByText('Try a search')).toBeInTheDocument()
  })
})
