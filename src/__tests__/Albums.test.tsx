import { render, screen } from '@testing-library/react'
import Albums from '@components/Albums'
import albumData from '@mocks/api/album.json'

const mockedNavigate = jest.fn()

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedNavigate,
}))

describe('<Albums />', () => {
  beforeAll(() => jest.spyOn(window, 'fetch'))

  it('should render album list', async () => {
    window.fetch = jest.fn().mockResolvedValueOnce({
      ok: true,
      json: async () => albumData,
    })

    render(<Albums id='7eBQrhxTHcor6gcbcLhqE5' />)

    expect(window.fetch).toHaveBeenCalledTimes(1)
    expect(await screen.findByText(albumData.items[0].name)).toBeInTheDocument()
  })
})
