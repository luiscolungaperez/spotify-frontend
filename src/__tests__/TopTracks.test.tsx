import { render, screen } from '@testing-library/react'
import TopTracks from '@components/TopTracks'
import tracks from '@mocks/api/toptracks.json'

const mockedNavigate = jest.fn()

jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedNavigate,
}))

describe('<TopTracks />', () => {
  beforeAll(() => jest.spyOn(window, 'fetch'))

  it('should render top tracks', async () => {
    window.fetch = jest.fn().mockResolvedValueOnce({
      ok: true,
      json: async () => tracks,
    })

    render(<TopTracks id='7eBQrhxTHcor6gcbcLhqE5' />)

    expect(window.fetch).toHaveBeenCalledTimes(1)
    expect(await screen.findByText(tracks.tracks[0].name)).toBeInTheDocument()
  })
})
