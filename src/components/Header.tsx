import { Menu, Transition } from '@headlessui/react'
import { Fragment } from 'react'

function Header({ user }: { user: TUser | any }) {
  return (
    <header className='w-11/12 ml-12 h-20 bg-transparent flex items-center justify-start text-white'>
      {Object.keys(user).length > 0 && (
        <Menu as='div' className='relative inline-block text-left'>
          <div>
            <Menu.Button className='inline-flex justify-center items-center w-full px-4 py-2 text-sm font-medium text-white bg-black rounded-full bg-opacity-20 hover:bg-opacity-30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75'>
              <img
                className='h-10 w-10 object-cover rounded-full mr-2'
                src={user.images[0].url}
                alt={user?.display_name}
              />
              <p>{user?.display_name}</p>
            </Menu.Button>
          </div>
          <Transition
            as={Fragment}
            enter='transition ease-out duration-100'
            enterFrom='transform opacity-0 scale-95'
            enterTo='transform opacity-100 scale-100'
            leave='transition ease-in duration-75'
            leaveFrom='transform opacity-100 scale-100'
            leaveTo='transform opacity-0 scale-95'>
            <Menu.Items className='absolute left-0 w-56 mt-2 origin-top-right bg-white divide-y divide-gray-100 rounded-md shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none'>
              <div className='px-1 py-1 bg-card'>
                <Menu.Item>
                  {({ active }) => (
                    <button
                      className={`${
                        active ? 'bg-white text-card' : 'text-white'
                      } group flex rounded-md items-center w-full px-2 py-2 text-sm`}
                      onClick={() => {
                        window.localStorage.clear()
                        window.location.replace('/')
                      }}>
                      Log out
                    </button>
                  )}
                </Menu.Item>
              </div>
            </Menu.Items>
          </Transition>
        </Menu>
      )}
    </header>
  )
}

export default Header
