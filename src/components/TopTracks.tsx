import { useEffect, useState } from 'react'
import { getTopTracks } from '@/api/calls'
import Card from './Card'

interface TopTrackProps {
  id: string | undefined
}

function TopTracks({ id }: TopTrackProps) {
  const [topTracks, setTopTracks] = useState<TTracksResponse[]>([])

  useEffect(() => {
    getTopTracks(id, window.localStorage.getItem('accessToken')).then(
      ({ tracks }: { tracks: TTracksResponse[] }) => setTopTracks(tracks),
    )
  }, [id])

  return (
    <article className='container mx-auto mt-12'>
      <h2 className='text-2xl font-bold text-center md:text-left text-white mb-8'>
        Top Tracks
      </h2>
      <section className='grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-6 gap-4'>
        {topTracks?.map((track: TTracksResponse) => (
          <Card key={`${track.id}__topTrack`} track={track}>
            <div className='text-gray-400 text-sm'>
              <h3
                className=' text-white truncate w-full text-lg'
                title={track.name}>
                {track.name}
              </h3>
              <p>
                {track.artists.map((artist: TArtistTrack) => {
                  let artists = ''
                  artists += artist.name + ' • '
                  return artists
                })}
              </p>
              <p>Song</p>
            </div>
          </Card>
        ))}
      </section>
    </article>
  )
}

export default TopTracks
