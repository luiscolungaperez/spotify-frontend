import { useEffect, useState } from 'react'
import { getArtist } from '@/api/calls'

function ArtistHeader({ id }: { id: string | undefined }) {
  const [artist, setArtist] = useState<TArtist | null>(null)

  useEffect(() => {
    if (id)
      getArtist(id, window.localStorage.getItem('accessToken')).then(
        (data: TArtist) => setArtist(data),
      )
  }, [id])

  return (
    <div className='grid grid-cols-1 md:grid-cols-[13rem_1fr] max-h-80'>
      <img
        className='object-cover place-self-center w-52 h-52 md:h-[13rem] md:w-full rounded-full'
        src={artist?.images[0].url}
        alt={artist?.name}
        title={artist?.name}
      />
      <div className='text-white text-center md:justify-self-start md:self-center md:px-8 md:text-left'>
        <h2 className='text-8xl font-bold'>{artist?.name}</h2>
        <p className='text-xl'>Seguidores: {artist?.followers.total}</p>
      </div>
    </div>
  )
}

export default ArtistHeader
