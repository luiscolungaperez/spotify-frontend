import { getAlbums } from '@/api/calls'
import { useEffect, useState } from 'react'
import Card from './Card'

function Albums({ id }: { id: string | undefined }) {
  const [albums, setAlbums] = useState<TAlbum[]>([])

  useEffect(() => {
    getAlbums(id, window.localStorage.getItem('accessToken')).then(
      (data: TAlbums) => setAlbums(data.items),
    )
  }, [id])

  return (
    <article className='container mx-auto mt-12'>
      <h2 className='text-2xl font-bold text-center md:text-left text-white mb-8'>
        Albums
      </h2>
      <section className='grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-6 gap-4'>
        {albums?.map((album: TAlbum) => (
          <Card key={`${album.id}__album`} album={album}>
            <div className='text-gray-400 text-sm'>
              <h3
                className=' text-white truncate w-full text-lg'
                title={album.name}>
                {album.name}
              </h3>
              <p>{album.release_date}</p>
              <p>Album</p>
            </div>
          </Card>
        ))}
      </section>
    </article>
  )
}

export default Albums
