function timeFormat(time: number | undefined) {
  if (!time) {
    return '00:00'
  }
  let min = Math.floor((time / 1000 / 60) << 0)
  let sec = Math.floor((time / 1000) % 60)
  return `${min}:${sec < 10 ? '0' + sec : sec}`
}

function Song({ track }: { track?: TAlbumTrack }) {
  return (
    <div
      // eslint-disable-next-line jsx-a11y/aria-role
      role={track?.id}
      title={track?.name}
      className='text-white w-full h-16 flex items-center justify-between hover:bg-gray-800 px-4 my-8'>
      <h3 className='text-xl'>{track?.name}</h3>
      <p className='text-sm'>{timeFormat(track?.duration_ms)}</p>
    </div>
  )
}

export default Song
