import { useNavigate } from 'react-router-dom'
import logo from '../assets/music.png'

interface CardProps {
  artist?: TArtist
  track?: TTracksResponse
  album?: TAlbum
  children: React.ReactNode
}

function Card({ artist, track, album, children }: CardProps) {
  const navigate = useNavigate()
  const handleArtistClick = () => navigate(`/artist/${artist?.id}`)
  const handleAlbumClick = () => navigate(`/album/${album?.id}`)

  return (
    <>
      {artist && (
        <div
          role={artist.id}
          onClick={handleArtistClick}
          className='place-self-center bg-card w-52 p-4 rounded-md hover:bg-gray-800 cursor-pointer'>
          {artist.images.length > 0 ? (
            <img
              className='object-cover w-72 h-44 rounded-full'
              src={artist.images[0].url}
              alt={artist.name}
              title={artist.name}
            />
          ) : (
            <img
              style={{ filter: 'invert(0.4)' }}
              className='object-cover w-72 h-44 rounded-full'
              src={logo}
              alt={artist.name}
              title={artist.name}
            />
          )}

          <div className='mt-4'>{children}</div>
        </div>
      )}
      {track && (
        <div
          role={track.id}
          // onClick={handleArtistClick}
          className='place-self-center bg-card w-52 p-4 rounded-md hover:bg-gray-800 cursor-pointer'>
          <img
            className='object-cover w-72 h-44 rounded-full'
            src={track.album.images[1].url}
            alt={track.name}
            title={track.name}
          />
          <div className='mt-4'>{children}</div>
        </div>
      )}
      {album && (
        <div
          role={album.id}
          onClick={handleAlbumClick}
          className='place-self-center bg-card w-52 p-4 rounded-md hover:bg-gray-800 cursor-pointer'>
          <img
            className='object-cover w-72 h-44 rounded-full'
            src={album.images[1].url}
            alt={album.name}
            title={album.name}
          />
          <div className='mt-4'>{children}</div>
        </div>
      )}
    </>
  )
}

export default Card
