import { useEffect, useState } from 'react'
import { getArtists } from '@api/calls'
import Card from '@components/Card'

function Search() {
  const [artists, setArtists] = useState<TArtist[]>([])
  const [textSearch, setTextSearch] = useState<string>('')

  useEffect(() => {
    if (textSearch === '') {
      setArtists([])
    } else {
      getArtists(window.localStorage.getItem('accessToken'), textSearch).then(
        (data: TSearchResponse) => setArtists(data.artists.items),
      )
    }
  }, [textSearch])

  const handleSearch = (e: any) => setTextSearch(e.target.value)

  return (
    <article className='container mx-auto'>
      <div className='w-full flex flex-col items-center'>
        <h2 className='text-3xl font-bold text-center md:text-left text-white mb-4'>
          Search
        </h2>
        <input
          type='text'
          placeholder='Search by artist'
          className='py-1 px-2 rounded-2xl mb-8 outline-none focus:caret-indigo-500 focus:border-4 focus:border-indigo-500'
          onChange={handleSearch}
        />
      </div>
      <section className='grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 xl:grid-cols-6 gap-4'>
        {artists.length > 0 ? (
          <>
            {artists.map((artist: TArtist) => (
              <Card key={`${artist.id}__artist`} artist={artist}>
                <div className='text-gray-400 text-sm'>
                  <h3
                    className=' text-white truncate w-full text-lg'
                    title={artist.name}>
                    {artist.name}
                  </h3>
                  <p>Artist</p>
                </div>
              </Card>
            ))}
          </>
        ) : (
          <div className='w-full text-center'>
            <h2 className='text-gray-400 text-2xl'>Try a search</h2>
          </div>
        )}
      </section>
    </article>
  )
}

export default Search
