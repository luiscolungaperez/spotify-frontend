type TImage = {
  height: number
  url: string
  width: number
}

type TArtist = {
  external_urls: {
    spotify: string
  }
  followers: {
    href: null
    total: number
  }
  genres: string[]
  href: string
  id: string
  images: TImage[]
  name: string
  popularity: number
  type: string
  uri: string
}

type TSearchResponse = {
  artists: {
    items: TArtist[]
    href: string
    limit: number
    next: string
    offset: number
    previous: string
    total: number
  }
}

type TArtistTrack = {
  external_urls: {
    spotify: string
  }
  href: string
  id: string
  name: string
  type: string
  uri: string
}

type TArtistImage = {
  height: number
  url: string
  width: number
}

type TTracksResponse = {
  album: {
    album_type: string
    artists: TArtistTrack[]
    external_urls: {
      spotify: string
    }
    href: string
    id: string
    images: TArtistImage[]
    name: string
    release_date: string
    release_date_precision: string
    total_tracks: number
    type: string
    uri: string
  }
  artists: TArtistTrack[]
  disc_number: number
  duration_ms: number
  explicit: boolean
  external_ids: {
    isrc: string
  }
  external_urls: {
    spotify: string
  }
  href: string
  id: string
  is_local: boolean
  is_playable: boolean
  name: string
  popularity: number
  preview_url: string
  track_number: number
  type: string
  uri: string
}

type TUser = {
  country: string
  display_name: string
  email: string
  explicit_content: {
    filter_enabled: boolean
    filter_locked: boolean
  }
  external_urls: {
    spotify: string
  }
  followers: {
    href: null
    total: number
  }
  href: string
  id: string
  images: TImage[]
  product: string
  type: string
  uri: string
}

type TAlbum = {
  album_group: string
  album_type: string
  artists: TArtistTrack[]
  available_markets: string[]
  external_urls: {
    spotify: string
  }
  href: string
  id: string
  images: TImage[]
  name: string
  release_date: string
  release_date_precision: string
  total_tracks: number
  type: string
  uri: string
}

type TAlbums = {
  href: string
  items: TAlbum[]
  limit: number
  next: string
  offset: number
  previous: string
  total: number
}

type TCopyright = {
  text: string
  type: string
}

type TAlbumTrack = {
  artists: TArtistTrack[]
  available_markets: string[]
  disc_number: number
  duration_ms: number
  explicit: boolean
  external_urls: {
    spotify: string
  }
  href: string
  id: string
  is_local: boolean
  name: string
  preview_url: string
  track_number: number
  type: string
  uri: string
}

type TAlbumResponse = {
  album_type: string
  artists: TArtistTrack[]
  available_markets: string[]
  copyrights: TCopyright[]
  external_ids: {
    upc: string
  }
  external_urls: {
    spotify: string
  }
  genres: string[]
  href: string
  id: string
  images: TImage[]
  label: string
  name: string
  popularity: number
  release_date: string
  release_date_precision: string
  total_tracks: number
  tracks: {
    href: string
    items: TAlbumTrack[]
    limit: number
    next: string
    offset: number
    previous: string
    total: number
  }
  type: string
  uri: string
}
