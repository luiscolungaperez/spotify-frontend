import { loginUrl } from '@utils/urls'
import { useEffect } from 'react'

function Login() {
  const handleClickLogin = () => (window.location.href = loginUrl)

  useEffect(() => {
    window.localStorage.clear()
  }, [])

  return (
    <section className='w-screen h-screen flex items-center justify-center flex-col'>
      <h1 className='text-white font-bold text-3xl my-8'>
        Welcome to Spotify API
      </h1>
      <button
        onClick={handleClickLogin}
        type='button'
        className='bg-green-600 text-white px-6 py-2 rounded-xl hover:bg-green-900 transition ease-in-out text-xl'>
        Login with Spotify
      </button>
    </section>
  )
}

export default Login
