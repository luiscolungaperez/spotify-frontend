import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { getAlbum } from '@api/calls'
import Song from '@components/Song'

function Album() {
  const { id } = useParams()
  const [album, setAlbum] = useState<TAlbumResponse | null>(null)

  useEffect(() => {
    getAlbum(id, window.localStorage.getItem('accessToken')).then(
      (data: TAlbumResponse) => setAlbum(data),
    )
  }, [id])

  return (
    <article className='container mx-auto my-8'>
      <div className='grid grid-cols-1 md:grid-cols-[13rem_1fr] max-h-80'>
        <img
          className='object-cover place-self-center w-52 h-52 md:h-[13rem] md:w-full rounded-full'
          src={album?.images[0].url}
          alt={album?.name}
          title={album?.name}
        />
        <div className='text-white text-center md:justify-self-start md:self-center md:px-8 md:text-left'>
          <h2 className='text-4xl md:text-5xl font-bold'>{album?.name}</h2>
          <p className='text-3xl'>{album?.artists[0].name}</p>
          <p>
            Songs: {album?.total_tracks} • Year:{' '}
            {album?.release_date.split('-')[0]}
          </p>
        </div>
      </div>
      {album?.tracks.items.map((track: TAlbumTrack) => (
        <Song key={track.id} track={track} />
      ))}
    </article>
  )
}

export default Album
