import { useParams } from 'react-router-dom'
import TopTracks from '@components/TopTracks'
import ArtistHeader from '@components/ArtistHeader'
import Albums from '@components/Albums'

function Artist() {
  const { id } = useParams()

  return (
    <div className='container mx-auto my-8'>
      <ArtistHeader id={id} />
      <TopTracks id={id} />
      <Albums id={id} />
    </div>
  )
}

export default Artist
