import { useEffect, useState } from 'react'
import Search from '@/components/Search'
import useAuth from '../hooks/useAuth'
import SpotifyWebApi from 'spotify-web-api-node'
import Header from '@/components/Header'
import { Outlet, useLocation } from 'react-router-dom'

const spotifyApi = new SpotifyWebApi({
  clientId: process.env.REACT_APP_SPOTIFY_CLIENT_ID,
})

function Home({ code }: { code: string | null }) {
  const location = useLocation()
  const accessToken = useAuth(code)
  const [token, setToken] = useState<string | null>(null)
  const [user, setUser] = useState<any>({})

  useEffect(() => {
    if (!accessToken) return
    window.localStorage.setItem('accessToken', accessToken)
  }, [accessToken])

  useEffect(() => {
    setToken(window.localStorage.getItem('accessToken'))
    if (token) {
      spotifyApi.setAccessToken(token)
      spotifyApi
        .getMe()
        .then(({ body }: any) => {
          setUser(body)
        })
        .catch(() => {
          window.localStorage.clear()
          window.location.href = '/'
        })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [token, window.localStorage.getItem('accessToken')])

  return (
    <div>
      <Header user={user} />
      {location.pathname === '/' ? <Search /> : <Outlet />}
    </div>
  )
}

export default Home
