import { useEffect, useState } from 'react'

export default function useAuth(code: string | null) {
  const [accessToken, setAccessToken] = useState()
  const [refreshToken, setRefreshToken] = useState()
  const [expiresIn, setExpiresIn] = useState()

  useEffect(() => {
    if (!code) return

    window
      .fetch(`http://localhost:8000/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ code: code }),
      })
      .then((res) => res.json())
      .then((data) => {
        window.history.pushState({}, '', '/')
        setAccessToken(data.accessToken)
        setRefreshToken(data.refreshToken)
        setExpiresIn(data.expiresIn)
      })
      .catch((err) => {
        console.log(err)
        window.location = '/' as any
      })
  }, [code])

  useEffect(() => {
    if (!refreshToken || !expiresIn) {
      return
    }

    let interval = setInterval(() => {
      window
        .fetch(`http://localhost:8000/refresh`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({ refreshToken: refreshToken }),
        })
        .then((res) => res.json())
        .then((data) => {
          setAccessToken(data.accessToken)
          setExpiresIn(data.expiresIn)
        })
        .catch((err) => {
          console.log(err)
          window.location = '/' as any
        })
    }, (expiresIn - 60) * 1000) // 1 min before expire Time and multiplying it with 1000 becoz to convert it in miliseconds
    // This will make sure that if for some reason our refreshtoken or expireTime changes before an actual Refresh then it will clear the interval so that we don't use the incorrect expireTime or refreshtoken
    return () => clearInterval(interval)
  }, [refreshToken, expiresIn])

  return accessToken
}
