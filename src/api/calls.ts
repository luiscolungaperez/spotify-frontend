export async function getArtists(token: string | null, search: string) {
  const response = await fetch(
    `https://api.spotify.com/v1/search?q=${search}&type=artist`,
    {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  )
  const data = await response.json()
  return data
}

export async function getArtist(artistId: string, token: string | null) {
  const response = await fetch(
    `https://api.spotify.com/v1/artists/${artistId}`,
    {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  )

  const data = await response.json()
  return data
}

export async function getTopTracks(
  artistId: string | undefined,
  token: string | null,
) {
  if (!artistId) return
  const response = await fetch(
    `https://api.spotify.com/v1/artists/${artistId}/top-tracks?market=MX`,
    {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  )

  const data = await response.json()
  return data
}

export async function getAlbums(
  artistId: string | undefined,
  token: string | null,
) {
  if (!artistId) return
  const response = await fetch(
    `https://api.spotify.com/v1/artists/${artistId}/albums`,
    {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    },
  )

  const data = await response.json()
  return data
}

export async function getAlbum(
  albumId: string | undefined,
  token: string | null,
) {
  const response = await fetch(`https://api.spotify.com/v1/albums/${albumId}`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`,
    },
  })

  const data = await response.json()
  return data
}
