const endpoint = 'https:accounts.spotify.com/authorize'
const {
  REACT_APP_SPOTIFY_CLIENT_ID,
  REACT_APP_SPOTIFY_CALLBACK_HOST
} = process.env

const scopes = [
  "streaming",
  "user-read-email",
  "user-read-private",
];

export const loginUrl = `${endpoint}?client_id=${REACT_APP_SPOTIFY_CLIENT_ID}&redirect_uri=${REACT_APP_SPOTIFY_CALLBACK_HOST}&scope=${scopes.join('%20')}&response_type=code`
