import { StrictMode } from 'react'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import ReactDOM from 'react-dom'
import reportWebVitals from '@/reportWebVitals'
import Home from '@pages/Home'
import Login from '@pages/Login'
import Artist from '@pages/Artist'
import Album from '@pages/Album'
import '@/index.css'

const codeUrl = new URLSearchParams(window.location.search).get('code')
const token = window.localStorage.getItem('accessToken')

function App() {
  return (
    <Router>
      <Routes>
        <Route
          path='/'
          element={!token && !codeUrl ? <Login /> : <Home code={codeUrl} />}>
          <Route
            path='artist/:id'
            element={!token && !codeUrl ? <Login /> : <Artist />}
          />
          <Route
            path='album/:id'
            element={!token && !codeUrl ? <Login /> : <Album />}
          />
        </Route>
        <Route
          path='*'
          element={!token && !codeUrl ? <Login /> : <Home code={codeUrl} />}
        />
      </Routes>
    </Router>
  )
}

ReactDOM.render(
  <StrictMode>
    <App />
  </StrictMode>,
  document.getElementById('root'),
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()
