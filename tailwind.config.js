module.exports = {
  content: ['./src/**/*.{js,jsx,ts,tsx}'],
  theme: {
    extend: {
      colors: {
        card: '#181818',
      },
    },
  },
  plugins: [],
}
